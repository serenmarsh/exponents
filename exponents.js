// Seren
// 15/03
// 10010681

// declaring variables



// Heading
console.log("N      Square      cube    ");
console.log("-      ------      ----    ");

//for loop
console.log("-->   for loop   <--");
for ( x = 1; x <= 10; x++) {
    console.log(`${x}       ${x**2}          ${x**3}` );
}

//while loop
console.log("-->   while loop   <--");
x = 1
while ( x <= 10) {
    console.log(`${x}       ${x**2}          ${x**3}` );
    x++
}

//do while loop
console.log("-->   do while loop   <--");
x = 1
do {
    console.log(`${x}       ${x**2}          ${x**3}` );
    x++
}
while ( x <= 10);